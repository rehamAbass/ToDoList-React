import './ColorClasses.css'

export const colorsArray = [
    'hotpink', 'gold', 'coral',
    'cornflowerblue', 'cornsilk', 'dodgerblue',
    'darkgoldenred', 'magenta', 'lightmagenta',
    'mediumturquoise', 'mediumslateblue', 'mediumseagreen',
    'moccasin', 'rosybrown', 'royalblue',
    'palegoldenrod', 'plum', 'palevioletred',
    'papayawhip', 'palegreen',
]

export default colorsArray;